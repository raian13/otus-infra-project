repositories: 
# To use official "stable" charts a.k.a https://github.com/helm/charts/tree/master/stable
- name: elastic
  url: https://helm.elastic.co
- name: prometheus-community
  url: https://prometheus-community.github.io/helm-charts
- name: fluent
  url: https://fluent.github.io/helm-charts

helmDefaults:
  verify: false
  wait: false
  waitForJobs: false

commonLabels: {}

releases:
  - name: elasticsearch
    namespace: observability
    createNamespace: true
    chart: elastic/elasticsearch
    # version: 
    values:
      - elasticsearch.values.yaml
      
  - name: kibana
    namespace: observability
    createNamespace: true
    chart: elastic/kibana
    needs:
      - elasticsearch
    values:
      - kibana.values.yaml
      - ingress:
          hosts:
            - kibana.{{ requiredEnv "EXTERNAL_IP"   }}.nip.io

  - name: fluent-bit
    namespace: observability
    createNamespace: true
    chart: fluent/fluent-bit
    needs:
      - elasticsearch
    values: 
      - fluentbit.values.yaml

  - name: prometheus-operator
    namespace: observability
    createNamespace: true
    chart: prometheus-community/kube-prometheus-stack
    version: 17.0.3
    disableValidation: true
    disableValidationOnInstall: true
    values:
      - prometheus-operator.values.yaml
      - grafana:
          ingress:
            hosts:
              - grafana.{{ requiredEnv "EXTERNAL_IP" }}.nip.io
      - prometheus:
          ingress:
            enabled: true
            hosts:
              - prometheus.{{ requiredEnv "EXTERNAL_IP" }}.nip.io
      - alertmanager:
          ingress:
            enabled: true
            hosts:
              - alertmanager.{{ requiredEnv "EXTERNAL_IP" }}.nip.io
    hooks:
    # Create CRDs separately in helmfile presync hooks
    # https://github.com/roboll/helmfile/issues/1124
    # https://github.com/helm/helm/issues/7449
    # https://github.com/cloudposse/helmfiles/blob/59490fd2599d6113a14103be919985f9fbcea73a/releases/prometheus-operator.yaml
    # Hooks associated to presync events are triggered before each release is applied to the remote cluster.
    # This is the ideal event to execute any commands that may mutate the cluster state as it
    # will not be run for read-only operations like lint, diff or template.
    # These hook install the prometheuses.monitoring.coreos.com CustomResourceDefinition if needed
    - events: ["presync"]
      command: "/bin/sh"
      args: ["-c", "kubectl get crd prometheuses.monitoring.coreos.com >/dev/null 2>&1 || \
             { helm pull prometheus-community/kube-prometheus-stack --version {{`{{ .Release.Version }}`}} && tar -Oxzf kube-prometheus-stack-{{`{{ .Release.Version }}`}}.tgz kube-prometheus-stack/crds/crd-prometheuses.yaml | kubectl apply -f -; }"]
    # This hoook installs the alertmanagers.monitoring.coreos.com CustomResourceDefinition if needed
    - events: ["presync"]
      command: "/bin/sh"
      args: ["-c", "kubectl get crd alertmanagers.monitoring.coreos.com >/dev/null 2>&1 || \
             { helm pull prometheus-community/kube-prometheus-stack --version {{`{{ .Release.Version }}`}} && tar -Oxzf kube-prometheus-stack-{{`{{ .Release.Version }}`}}.tgz kube-prometheus-stack/crds/crd-alertmanagers.yaml | kubectl apply -f -; }"]
    # This hoook installs the alertmanagerconfigs.monitoring.coreos.com CustomResourceDefinition if needed
    - events: ["presync"]
      command: "/bin/sh"
      args: ["-c", "kubectl get crd alertmanagerconfigs.monitoring.coreos.com >/dev/null 2>&1 || \
             { helm pull prometheus-community/kube-prometheus-stack --version {{`{{ .Release.Version }}`}} && tar -Oxzf kube-prometheus-stack-{{`{{ .Release.Version }}`}}.tgz kube-prometheus-stack/crds/crd-alertmanagerconfigs.yaml | kubectl apply -f -; }"]
    # This hoook installs the prometheusrules.monitoring.coreos.com CustomResourceDefinition if needed
    - events: ["presync"]
      command: "/bin/sh"
      args: ["-c", "kubectl get crd prometheusrules.monitoring.coreos.com >/dev/null 2>&1 || \
             { helm pull prometheus-community/kube-prometheus-stack --version {{`{{ .Release.Version }}`}} && tar -Oxzf kube-prometheus-stack-{{`{{ .Release.Version }}`}}.tgz kube-prometheus-stack/crds/crd-prometheusrules.yaml | kubectl apply -f -; }"]
    # This hoook installs the servicemonitors.monitoring.coreos.com CustomResourceDefinition if needed
    - events: ["presync"]
      command: "/bin/sh"
      args: ["-c", "kubectl get crd servicemonitors.monitoring.coreos.com >/dev/null 2>&1 || \
             { helm pull prometheus-community/kube-prometheus-stack --version {{`{{ .Release.Version }}`}} && tar -Oxzf kube-prometheus-stack-{{`{{ .Release.Version }}`}}.tgz kube-prometheus-stack/crds/crd-servicemonitors.yaml | kubectl apply -f -; }"]
    # This hoook installs the podmonitors.monitoring.coreos.com CustomResourceDefinition if needed
    - events: ["presync"]
      command: "/bin/sh"
      args: ["-c", "kubectl get crd podmonitors.monitoring.coreos.com >/dev/null 2>&1 || \
             { helm pull prometheus-community/kube-prometheus-stack --version {{`{{ .Release.Version }}`}} && tar -Oxzf kube-prometheus-stack-{{`{{ .Release.Version }}`}}.tgz kube-prometheus-stack/crds/crd-podmonitors.yaml | kubectl apply -f -; }"]
    # This hoook installs the thanosrulers.monitoring.coreos.com CustomResourceDefinition if needed
    - events: ["presync"]
      command: "/bin/sh"
      args: ["-c", "kubectl get crd thanosrulers.monitoring.coreos.com >/dev/null 2>&1 || \
             { helm pull prometheus-community/kube-prometheus-stack --version {{`{{ .Release.Version }}`}} && tar -Oxzf kube-prometheus-stack-{{`{{ .Release.Version }}`}}.tgz kube-prometheus-stack/crds/crd-thanosrulers.yaml | kubectl apply -f -; }"]
    # This hoook installs the probes.monitoring.coreos.com CustomResourceDefinition if needed
    - events: ["presync"]
      command: "/bin/sh"
      args: ["-c", "kubectl get crd probes.monitoring.coreos.com >/dev/null 2>&1 || \
             { helm pull prometheus-community/kube-prometheus-stack --version {{`{{ .Release.Version }}`}} && tar -Oxzf kube-prometheus-stack-{{`{{ .Release.Version }}`}}.tgz kube-prometheus-stack/crds/crd-probes.yaml | kubectl apply -f -; }"]

  - name: elasticsearch-exporter
    namespace: observability
    createNamespace: true
    chart: prometheus-community/prometheus-elasticsearch-exporter
    disableValidation: true
    disableValidationOnInstall: true
    needs: 
      - observability/prometheus-operator
    values:
      - elasticsearch-exporter.values.yaml
    set:
      - name: es.uri
        value: 'http://elasticsearch-master:9200'

# helmfiles:
#   - path: ../prepare/helmfile.yaml
