# otus-infra-project

## Компоненты проекта

- Managed Kubernetes Service на Yandex.Cloud
- docker image для управления и развертывания приложения
- Nginx ingress controller
- Prometheus, Grafana, AlertManager
- Elasticsearch + FluentBit
- приложение hipster-shop

## Docker image helm-tools

Образ включает:

- helm
- helmfile
- kubectl
- yc (консольная утилита Yandex.Cloud)

Dockerfile лежит в utils. Сборка:

```bash
docker build -t REPO_NAME/helm-tools:1.0.0 . 
```

## Gitlab CI

Pipeline описан в .gitlab-ci.yaml
Сборка образов запускается только для тегированных коммитов.
Переменные для доступа к Яндекс.Облаку и docker registry - размещаются в переменных GitLab и доступны мейнтейнеру проекта.

## Порядок развертывания

### Кластер Kubernetes

Развертывание локально, с помощью Terraform.
Requirements:

- скрипты предназначены для Linux/MacOS
- установлена и сконфигурирована утилита yc ([ссылка](https://cloud.yandex.ru/docs/cli/quickstart))
- установлена утилита jq
- установлен terraform

Формируем файл переменных (в git repo не попадает)

```bash
./create-variables.sh > terraform.tfvars
```

Инициализируем доступ к Yandex.cloud

```bash
yc init
```

Получаем токен и выполняем развертывание с использованием этого токена:

```bash
export IAMTOKEN=`yc iam create-token`
terraform init
terraform plan -var="iam_token=$IAMTOKEN"
terraform apply -var="iam_token=$IAMTOKEN" -auto-approve
```

### Настройка k8s, установка GAP-стека и стека логирования

Можно запустить pipeline в Gitlab, job "setup cluster" доступен из ветки devops

Хелмфайлы для nginx-ingress-controller и observability лежат в каталогах nginx-ingress и observability.

Если нужна установка вручную - то необходимо сначала установить observability/helmfile, потом nginx-ingress/helmfile, экспортировать переменную окружения EXTERNAL_IP с адресом LoadBalancer и повторно прогнать observability/helmfile для установки публично доступных адресов для Grafana, Prometheus, Kibana.

### CI/CD приложения

Билд и деплой приложения выполняются для тегированных версий. Используется fixed versioning для моно-репозитория - изменение в одном сервисе порождает новую версию для всех сервисов. 

Приложение деплоится в отдельный namespace с именем, зависящим от environment (apps-develop, apps-testing).
