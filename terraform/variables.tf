variable iam_token {
  type        = string
  default     = ""
  description = "Yandex.Cloud IAM token"
}

variable "cloudID" {
  type = string
  default = ""
  description = "Yandex.Cloud cloud id"
}
variable "folderID" {
  type = string
  default = ""
  description = "Yandex.Cloud folder id"
}
variable "networkID" {
  type = string
  default =  ""
  description = "Yandex.Cloud network id"
}
variable "subnetID" {
  type = list
  default = []
  description = "Yandex.Cloud subnet id"
}
variable "serviceAccount" {
  type = string
  default = ""
  description = "Yandex.Cloud serviceAccount id"
}
variable "kmsKey" {
  type = string
  default = ""
}
# variable "sshKey" {
#   type = string
#   default = ""
# }