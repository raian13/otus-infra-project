#!/bin/bash

# Получим ID 
CLOUD_ID=`yc config get cloud-id`
FOLDER_ID=`yc config get folder-id`
COMPUTE_ZONE=`yc config get compute-default-zone`

NETWORK_ID=`yc vpc network list --format json | jq -r '.[] | select (.name == "default") | .id'`

SUBNET_ID=`yc vpc network list-subnets --name default --format json |NETWORK_ID="$NETWORK_ID" jq '.[] | select(.zone_id == "ru-central1-a") | select (.network_id == env.NETWORK_ID) | .id'`

SA_ID=`yc iam service-account list --format json | jq '.[] | select (.name == "sa-resource") | .id'`

KMS_KEY=`yc kms symmetric-key list --format json | jq '.[] | select (.name == "test-key") | .id'`

echo "cloudID=\"$CLOUD_ID\""
echo "folderID=\"$FOLDER_ID\""
echo "networkID=\"$NETWORK_ID\""
echo "subnetID=[$SUBNET_ID]"
echo "serviceAccount=$SA_ID"
echo "kmsKey=$KMS_KEY"